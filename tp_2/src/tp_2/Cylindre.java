package tp_2;

public class Cylindre extends Cercle{
	public double hauteur;
	
	public Cylindre() {
		this.hauteur = 1.0;
	}
	
	public double getHauteur() {
		return this.hauteur;
	}
	
	public void setHauteur(double h) {
		this.hauteur = h;
		
	}
	
	public double calculerVolume() {
		return super.calculerAire() * getHauteur();
	}
	
	
	
	public void seDecrir() {
		System.out.println("Le cylindre d'hauteur : " + getHauteur() + ", de perimetre  " + super.calculerPerimetre() + " et de volume " + calculerVolume());	
		super.seDecrir();
	}
	
	
	public Cylindre(double r,double h, String couleur, boolean coloriage) {
		super(r,couleur,coloriage);
		setHauteur(h);
		
	}
}
