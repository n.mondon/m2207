package tp_2;

public class Cercle extends Form{
	private double rayon;
	
	public Cercle() {
		super();
		this.rayon = 1.0;
	
	}

	
	public double getRayon() {
		return rayon;
	}
	
	public void setRayon(double lr) {
		this.rayon = lr;
	}
	
	public void seDecrir() {
		System.out.println("Le cercle de rayon : " + getRayon() + ", d'aire " + calculerAire() + " et de périmètre " + calculerPerimetre());
		super.seDecrir();

	}
	
	public Cercle(double r) {
		setRayon(r);
	}
	
	public Cercle(double r, String couleur, boolean coloriage) {
		super(couleur,coloriage);
		setRayon(r);
		
	}
	
	public double calculerAire() {
		return Math.PI * getRayon() * getRayon();
	}
	
	public double calculerPerimetre() {
		return 2 * Math.PI * getRayon();
	}
	
	

}
